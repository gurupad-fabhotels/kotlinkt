package basics

class Person(var name: String, val age: Int)


fun main() {
    val person = Person("Jen", 10)

    println(person.name + person.age)

    person.name = "Ben"

    // Can use string templates
    println("Name : $person.name \n Age : $person.age")
}
