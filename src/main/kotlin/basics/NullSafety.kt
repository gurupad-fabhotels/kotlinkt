package basics

fun main() {
    // Nullable property
    var name: String? = null

    printDetails(name)

    name = getName()

    printDetails(name)

    // Safe access
    val namePairs = name?.split(" ")

    // Elvis operator
    var person: Person? = Person(name ?: "Empty", 10)

    printContents(person)
}

fun getName(): String? {
    print("Enter you full name : ")
    return readLine()
}

fun printDetails(name: String?) {
    println("Length - ${name?.length}, Name - $name")
}

fun printContents(thing: Any?) {
    // Smart casting
    if (thing is Person) {
        println(thing)
    }

    //Safe assignment
    val person: Person? = thing as? Person
    println(person?.age)
}