package basics

import java.util.*

abstract class Animal(val name: String, val legs: Int) {
    var distanceCovered: Int
        private set
        get() {
            println("$name has run for $field kms")
            return field
        }

    open val strength = 10

    init {
        distanceCovered = 0
    }

    fun runForX(x: Int) {
        distanceCovered += x
    }

    abstract fun greet(): String
}


//This is final by default
class Dog private constructor(name: String) : Animal(name, 2) {

    override val strength: Int = 100

    override fun greet(): String {
        return "Says bowww"
    }

    override fun toString(): String {
        return "$name says ${greet()}, it has $legs legs with strength $strength"
    }

    companion object {
        fun createDog(dogName: String): Dog = Dog(dogName)
    }
}

// No static methods, only top level functions
// If needed can use companion objects

fun main() {
    val dog = Dog.createDog("Max")
    println(dog.toString())
}

// About sealed classes
sealed class DIRECTION

class Left() : DIRECTION()
class Right() : DIRECTION()
class Top() : DIRECTION()
class Bottom() : DIRECTION()


fun moveObject(box: Any, direction: DIRECTION) {
    when (direction) {

        is Left -> TODO()
        is Right -> TODO()
        is Top -> TODO()
        is Bottom -> TODO()
    }
}


// Singleton
object ApiManager {

}


// Generics
open class List<T>(size: Int)

open class LinkedList<T>(size: Int) : List<T>(size)

