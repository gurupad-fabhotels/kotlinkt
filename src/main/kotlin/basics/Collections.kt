package basics

import java.util.function.Consumer

fun main() {

    // List creation
    val animals: List<String> = listOf("Dog", "Cat", "Giraffe", "Lion", "Snake")

    // Loop with in keyword (Needs iterator implemention)
    for (animal in animals) {
        println(animal)
    }

    //
    animals.forEach(object : Consumer<String> {
        override fun accept(t: String) {
            println(t)
        }
    })

    animals.forEach({ it: String ->
        println(it)
    })

    animals.forEach {
        when (it) {
            "Dog" -> println("Ima a doge")
            "Lion" -> println("Roooaaar")
            else -> println("?")
        }
    }


    val mixedStuff: List<Any> = listOf("Dog", 123, "Lion", Person("Ravi", 12))

    mixedStuff.forEach {
        when {
            it == "Dog" -> println("Ima a doge")
            it is Int -> println("Some number $it")

            isAPerson(it) -> {
                println("This is a person - ")
                printContents(it)
            }
        }
    }

    // Loop with index
    animals.forEachIndexed { index, s -> print("Animal at $index is $s") }

    // Ranges
    for (i in 0..3) {
        //
    }

    for (i in 2..100 step 2) {
        print(i)
    }


    for (i in 'a'..'z') {
        //
    }

    for (i in 100 downTo 2) {
        //
    }

    val persons = listOf<Person>() // Let's say it has lot's of persons

    val namesOfPeopleOlderThan25: List<String> = persons
        .filter { it.age > 25 }
        .map { it.name }

    val anyOldPeople: Boolean = persons.any { it.age > 60 }
    val areAllKids = persons.all { it.age < 12 }

}

fun isAPerson(it: Any): Boolean {
    return it is Person
}
