package basics

// Lambdas - Higher order function

// Syntax - (parameters) -> (ReturnType) {body}
val add: (x: Int, y: Int) -> Int = { x, y -> x + y }
val sub: (x: Int, y: Int) -> Int = { x, y -> x - y }

fun calculate(x: Int, y: Int, operation: (x: Int, y: Int) -> Int) {
    print(operation(x, y))
}

fun main() {
    calculate(1, 2, add)
    calculate(10, 2, sub)
}

