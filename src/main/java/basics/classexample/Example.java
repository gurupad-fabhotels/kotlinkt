package basics.classexample;

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }
}

public class Example {

    public static void main(String[] args) {
        Person person = new Person("Jen", 10);

        System.out.println(person.getName() + person.getAge());

        person.setName("Ben");

        System.out.println(person.getName() + person.getAge());
    }
}
