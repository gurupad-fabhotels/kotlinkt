package basics.functions;

class Person {
    private String name;
    private int age;

    private String nickName;


    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setName(String firstName, String middleName, String lastName, String nickName) {
        this.name = firstName + middleName + lastName;
        this.nickName = nickName;
    }

    public void setName(String firstName, String middleName, String lastName) {
        this.name = firstName + middleName + lastName;
    }

    public void setName(String firstName, String lastName) {
        this.name = firstName + lastName;
    }
}

public class Functions {

    public static void main(String[] args) {
        Person person = new Person("Jen", 10);

        System.out.println(person.getName() + " " + person.getAge());

        person.setName("Ben");

        System.out.println(person.getName() + person.getAge());

        person.setName("Rahul", "Sharma");

        System.out.println(person.getName());
    }
}
